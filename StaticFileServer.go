package bootstrapserver

import (
	"net/http"
	"log"
)

func Server(){
	http.Handle("/", http.FileServer(AssetFS()))
	if err := http.ListenAndServe(":13273", nil); err != nil {
		log.Fatal(err)
	}
}
