package main

import (
	"gitlab.com/spyglass/bootstrap"
	"time"
)

func main() {
	go bootstrapserver.Server()

	time.Sleep(10*time.Minute)
}
